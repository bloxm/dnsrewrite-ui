
##############################################
# OpenWrt Makefile for dnsrewrite-ui program
#
#
# Most of the variables used here are defined in
# the include directives below. We just need to 
# specify a basic description of the package, 
# where to build our program, where to find 
# the source files, and where to install the 
# compiled program on the router. 
# 
# Be very careful of spacing in this file.
# Indents should be tabs, not spaces, and 
# there should be no trailing whitespace in
# lines that are not commented.
# 
##############################################

include $(TOPDIR)/rules.mk

# Name and release number of this package
PKG_NAME:=dnsrewrite-ui
PKG_RELEASE:=1

# This specifies the directory where we're going to build the program.  
# The root build directory, $(BUILD_DIR), is by default the build_mipsel 
# directory in your OpenWrt SDK directory
PKG_BUILD_DIR := $(BUILD_DIR)/$(PKG_NAME)


include $(INCLUDE_DIR)/package.mk



# Specify package information for this program. 
# The variables defined here should be self explanatory.
# If you are running Kamikaze, delete the DESCRIPTION 
# variable below and uncomment the Kamikaze define
# directive for the description below
define Package/dnsrewrite-ui
	SECTION:=utils
	CATEGORY:=Utilities
	TITLE:=dnsrewrite-ui -- sorts the UI
	DEPENDS:=+dnsrewrite
endef

#To add a dependency, modify DEPENDS, above
#This adds a dependency to our custom library - +libspcdns+codec



# Uncomment portion below for Kamikaze and delete DESCRIPTION variable above
define Package/dnsrewrite-ui/description
endef


# Specify what needs to be done to prepare for building the package.
# In our case, we need to copy the source files to the build directory.
# This is NOT the default.  The default uses the PKG_SOURCE_URL and the
# PKG_SOURCE which is not defined here to download the source from the web.
# In order to just build a simple program that we have just written, it is
# much easier to do it this way.
define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/
endef


# We do not need to define Build/Configure or Build/Compile directives
# The defaults are appropriate for compiling a simple program such as this one


# Specify where and how to install the program. Since we only have one file, 
# the dnsrewrite executable, install it by copying it to the /bin directory on
# the router. The $(1) variable represents the root directory on the router running 
# OpenWrt. The $(INSTALL_DIR) variable contains a command to prepare the install 
# directory if it does not already exist.  Likewise $(INSTALL_BIN) contains the 
# command to copy the binary file from its current location (in our case the build
# directory) to the install directory.
define Package/dnsrewrite-ui/install
	$(INSTALL_DIR) $(1)/bin/bloxm/syscfg

	$(INSTALL_DIR) $(1)/www/css
	$(INSTALL_DIR) $(1)/www/fonts
	$(INSTALL_DIR) $(1)/www/js
#	$(INSTALL_DIR) $(1)/www/routerportal
#	$(INSTALL_DIR) $(1)/www/routerportal/js
#	$(INSTALL_DIR) $(1)/www/routerportal/js/plugins
	$(INSTALL_DIR) $(1)/www/adminportal/js
	$(INSTALL_DIR) $(1)/www/adminportal/js/routercontrol
	$(INSTALL_DIR) $(1)/www/adminportal/js/routercontrol/plugins
	$(INSTALL_DIR) $(1)/www/adminportal/assets
	$(INSTALL_DIR) $(1)/www/adminportal/templates
	$(INSTALL_DIR) $(1)/www/adminportal/cgi-bin
	$(INSTALL_DIR) $(1)/www/adminportal/cgi-bin/private
	$(INSTALL_DIR) $(1)/www/regportal/js
	$(INSTALL_DIR) $(1)/www/regportal/templates
	$(INSTALL_DIR) $(1)/www/regportal/cgi-bin
	$(INSTALL_DIR) $(1)/www/blockportal/js
	$(INSTALL_DIR) $(1)/www/blockportal/templates
	$(INSTALL_DIR) $(1)/www/blockportal/cgi-bin
	$(INSTALL_DIR) $(1)/www/holdportal/js
	$(INSTALL_DIR) $(1)/www/holdportal/templates
	$(INSTALL_DIR) $(1)/www/holdportal/cgi-bin

	$(INSTALL_BIN) $(PKG_BUILD_DIR)/syscfg/* $(1)/bin/bloxm/syscfg/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/scripts/*.sh $(1)/bin/bloxm/

	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/adminportal/*.html $(1)/www/adminportal/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/adminportal/assets/* $(1)/www/adminportal/assets/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/adminportal/templates/* $(1)/www/adminportal/templates/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/adminportal/js/*.js $(1)/www/adminportal/js/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/adminportal/js/routercontrol/*.js $(1)/www/adminportal/js/routercontrol/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/adminportal/js/routercontrol/plugins/*.js $(1)/www/adminportal/js/routercontrol/plugins/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/adminportal/cgi-bin/*.cgi $(1)/www/adminportal/cgi-bin/
	ln -s ../config.cgi $(1)/www/adminportal/cgi-bin/private/config.cgi

	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/blockportal/*.html $(1)/www/blockportal/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/blockportal/templates/* $(1)/www/blockportal/templates/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/blockportal/js/*.js $(1)/www/blockportal/js/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/blockportal/cgi-bin/* $(1)/www/blockportal/cgi-bin/

	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/holdportal/*.html $(1)/www/holdportal/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/holdportal/templates/* $(1)/www/holdportal/templates/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/holdportal/cgi-bin/* $(1)/www/holdportal/cgi-bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/holdportal/js/*.js $(1)/www/holdportal/js/

	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/regportal/*.html $(1)/www/regportal/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/regportal/templates/* $(1)/www/regportal/templates/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/regportal/js/*.js $(1)/www/regportal/js/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/regportal/cgi-bin/* $(1)/www/regportal/cgi-bin/

#	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/routerportal/*.html $(1)/www/routerportal/
#	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/routerportal/templates/* $(1)/www/routerportal/templates/
#	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/routerportal/js/*.js $(1)/www/routerportal/js/
#	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/routerportal/js/plugins/*.js $(1)/www/routerportal/js/plugins/
#	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/routerportal/cgi-bin/* $(1)/www/routerportal/cgi-bin/


	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/css/* $(1)/www/css/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/fonts/* $(1)/www/fonts/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/html/js/* $(1)/www/js/

endef

define Package/dnsrewrite-ui/postinst
#!/bin/bash

exec > /tmp/postinst-ui
exec 2>&1
set -x
env

if [[ "L${OPENWRT_BUILD}" == "L1" ]] ; then
    $${IPKG_INSTROOT}/bin/bloxm/syscfg/syscfg-ui.sh
else
    /bin/bloxm/syscfg/syscfg-ui.sh
fi

endef

define Package/dnsrewrite-ui/clean
endef

# This line executes the necessary commands to compile our program.
# The above define directives specify all the information needed, but this
# line calls BuildPackage which in turn actually uses this information to
# build a package.
$(eval $(call BuildPackage,dnsrewrite-ui))
