#!/bin/bash

exec > "${IPKG_INSTROOT}/tmp/syscfg-ui.sh"
exec 2>&1
set -x
readlink $0

if [[ "L${OPENWRT_BUILD}" == "L1" ]] ; then
    export ROOT_PREFIX="${IPKG_INSTROOT}"
fi


INSTALL_DIR="bin/bloxm"

INC="./${INSTALL_DIR}/dnsrewrite.inc"
#most important - set the symlink for /dnsrewrite.inc
ln -sf ${INC} ${ROOT_PREFIX}/

. ${ROOT_PREFIX}/dnsrewrite.inc

env

#Set up the symlinks for the javascript and css and fonts
ln -sf ../js ${ROOT_PREFIX}/www/regportal/libjs
ln -sf ../css ${ROOT_PREFIX}/www/regportal/css
ln -sf ../fonts ${ROOT_PREFIX}/www/regportal/fonts

ln -sf ../js ${ROOT_PREFIX}/www/adminportal/libjs
ln -sf ../css ${ROOT_PREFIX}/www/adminportal/css
ln -sf ../fonts ${ROOT_PREFIX}/www/adminportal/fonts


ln -sf ../js ${ROOT_PREFIX}/www/blockportal/libjs
ln -sf ../css ${ROOT_PREFIX}/www/blockportal/css
ln -sf ../fonts ${ROOT_PREFIX}/www/blockportal/fonts

ln -sf ../js ${ROOT_PREFIX}/www/holdportal/libjs
ln -sf ../css ${ROOT_PREFIX}/www/holdportal/css
ln -sf ../fonts ${ROOT_PREFIX}/www/holdportal/fonts

#iln -sf ../js ${ROOT_PREFIX}/www/routerportal/libjs
#ln -sf ../css ${ROOT_PREFIX}/www/routerportal/css
#ln -sf ../fonts ${ROOT_PREFIX}/www/routerportal/fonts

ln -sf . ${ROOT_PREFIX}/www/adminportal/blxadmin 
exit 0
