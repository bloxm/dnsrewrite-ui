#!/bin/bash

#TODO: For headless update we need to work out if a dev is logged on, and prevent it
#otherwise should dowload from main update site.

if [[ "${HEADLESS}" = "1" ]] ; then
    exit
fi


if ! grep force_space /etc/opkg.conf ; then
        echo "option force_space" >> /etc/opkg.conf
fi

opkg install http://192.168.0.25/packages/base/dnsrewrite-ui_1_ramips_24kec.ipk
