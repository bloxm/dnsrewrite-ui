var openwrt_router={};



openwrt_router.supportedModel=function()
{
	//Check self.modelNumber - self.firmwareVersion may also be available
	var self=this;
	console.log("Checking " + this.modelNumber);
	
	if(this.modelNumber.match(/OpenWrt.*15\.05*/))
	{
		console.log("MATCH " + this.modelNumber);
		return true;
	}
	console.log("NO MATCH " + this.modelNumber);
	return false;
}


openwrt_router.start=function(context)
{
	var self=this;
	self.context=context;
	self.networkId = "protectedlan";

	self.ip4Value=true;
	self.context.MaxReboots=1;
	self.bDidSomething=false;
	this._start()
}

openwrt_router.undo=function(context)
{
	var self=this;
	self.context=context;
	self.networkId = "protectedlan";

	self.ip4Value=false;
	self.context.MaxReboots=1;
	self.bDidSomething=false;
	this._start()
}

openwrt_router._start=function()
{
	console.log("WHERE " + __FUNCTION__ + "()  - " + __FILE__ + ":" + __LINE__);
	var self=this;

	self.routerInfo="OpenWrt";

	self.context.cb_status(self.context, self.routerInfo, "Looking for a OpenWRT router");
	
	self.checkVersion=function()
	{
	console.log("WHERE " + __FUNCTION__ + "()  - " + __FILE__ + ":" + __LINE__);
		//Get the model and firmware version
		//Now get the Model label.
		var elem = self.context.targetIFrame.contentWindow.document.getElementsByTagName("footer");
		if(elem && elem.length == 1)
		{
			var versionString = elem[0].textContent;
			self.modelNumber = versionString.trim();
			self.firmwareVersion = versionString.trim();
			self.routerInfo = "OpenWRT Router : " + self.modelNumber + " (" + self.firmwareVersion + ")";
			if(self.supportedModel())
			{
				self.context.cb_status(self.context, self.routerInfo, "Pairing with " + self.routerInfo);

				//Router is suported - clear the frame (required to remove previous overriden functions)
				self.context.targetIFrame.onload=self.startLogin; //this will kick off the DHCP IP4 setting
				self.context.targetIFrame.src="about:blank";
				return;
			}
			else
			{
				//Unsupported?
				self.context.cb_unsupported(self.context, "Unsupported model " + self.routerInfo);
			}
		}
		self.context.cb_unsupported(self.context, "Unsupported model " + self.routerInfo);
	}

	self.startVersionPage=function()
	{
	console.log("WHERE " + __FUNCTION__ + "()  - " + __FILE__ + ":" + __LINE__);
		if(self.context.rebootCount > self.context.MaxReboots)
		{
			self.context.cb_failed(self.context, "Too many reboots " + self.context.rebootCount);
			return;
		}

		self.context.targetIFrame.onload=self.checkVersion;

		//Start the load
		//Pass the credential on the first call only - rely on the browser to store them
		//This avoids an issue where Chrome refuses to send Auth header on the form POST, if the credentials are in the URL
		var URL=self.context.makeURL("cgi-bin/luci", false);
		self.context.targetIFrame.src=URL;
	}

	self.startLogin=function()
	{
	console.log("WHERE " + __FUNCTION__ + "()  - " + __FILE__ + ":" + __LINE__);
		self.context.cb_status(self.context, self.routerInfo, "Logging in to " + self.routerInfo, "Identification complete");

		self.context.targetIFrame.onload=self.loginOpenWrt

		//Start the load
		var URL=self.context.makeURL("cgi-bin/luci");
		self.context.targetIFrame.src=URL;
	}

	self.waitForStatusPage=function()
	{
	console.log("WHERE " + __FUNCTION__ + "()  - " + __FILE__ + ":" + __LINE__);
		//Check if the login was successful, if so, store the URL as it will be required for
		//seesion info for subsequent requests otherwise ask for password
		self.pathprefix=self.context.targetIFrame.contentWindow.location.pathname.substring(1);
		self.context.targetIFrame.onload=self.changeDHCP4Settings
		
		//Start the load
		var URL=self.context.makeURL(self.pathprefix + "/admin/network/network/" + self.networkId);
		console.log("About to set URL to " + URL);
		self.context.targetIFrame.src=URL;
		console.log("URL now set");
	}

	self.loginOpenWrt=function()
	{
	console.log("WHERE " + __FUNCTION__ + "()  - " + __FILE__ + ":" + __LINE__);
		var doc=self.context.targetIFrame.contentWindow.document;
		var inputs = doc.getElementsByTagName('input');
		var loginButton=null;

		for(var i=0; i < inputs.length; ++i)
		{
			if(inputs[i].type == "submit")
			{
				loginButton=inputs[i];
				break;
			}
		}

		if(loginButton)
		{
			self.context.targetIFrame.onload=self.waitForStatusPage;
	console.log("WHERE " + __FUNCTION__ + "()  - " + __FILE__ + ":" + __LINE__);
			self.context.mouseClick(doc, loginButton);
		}
		else
		{
			console.log("Could not find the Login button");
			self.context.cb_failed(self.context);
			return;
		}
	}

	self.pingRouter=function()
	{
		self.context.pingRouter("cgi-bin/luci", "cgi-bin/luci/admin/system/admin", self.startVersionPage, true);
	}



	self.waitForReboot=function(msg)
	{
		self.context.rebootCount++;
		self.context.rebootMsg=msg + " (Reboot count: " + self.context.rebootCount + ")";
		console.log("Wait for reboot count is now " + self.rebootCount);

		//Let the applicaiton do its thing
		self.context.targetIFrame.onload=self.isRebooted;

		self.context.cb_status(self.context, self.routerInfo, "Pairing with " + self.routerInfo, "Waiting for reboot (" + self.context.rebootCount + "/" + self.context.MaxReboots + ")");
	}

	self.isRebooted=function()
	{
		console.log("Waiting for reboot - " + self.context.targetIFrame.contentWindow.location);
				var URL=self.context.makeURL(self.pathprefix + "/admin/system/reboot?reboot=1");
		if(self.context.targetIFrame.contentWindow.location == self.context.makeURL(self.pathprefix + "/admin/system/reboot?reboot=1"))
		{
			//We have detected the router is rebooting - now we need to poll it.

			//Just wait 5 secs before changing the page so we can see it reboot for debug purposes
			setTimeout(function()
					{
						self.context.targetIFrame.onload=self.pingRouter;
						self.context.targetIFrame.src="about:blank";
					}, 5000);
		}
	}

	self.changeDHCP4Settings=function()
	{
	console.log("WHERE " + __FUNCTION__ + "()  - " + __FILE__ + ":" + __LINE__);
	console.log(self.context.targetIFrame.contentWindow.location);
	console.log( self.context.targetIFrame.src);
		var doc=self.context.targetIFrame.contentWindow.document;
		var ip4 = doc.getElementsByName('cbid.dhcp.' + self.networkId + '.ignore')[0];
		var bApply = false;

		//Check we have something to do
		if(ip4 !== undefined)
		{
			if(ip4.checked != self.ip4Value)
			{
				console.log("Setting DHCP4 to " + self.ip4Value);
				bApply = true;
				ip4.checked = self.ip4Value;
			}
			else
			{
				console.log("DHCP4 nothing to do");
				if(self.context.rebootCount == 0)
				{
					//reboot
					self.waitForReboot("Applied DHCP4 Settings");
					var URL=self.context.makeURL(self.pathprefix + "/admin/system/reboot?reboot=1");
					console.log("About to set URL to " + URL);
					self.context.targetIFrame.src=URL;
					console.log("URL now set");
					return;
				}
				//Do nothing - falls through to cbSuccess
			}
		}
		else
		{
			console.log("Could not find the ip4 DHCP checkbox");
			self.context.cb_failed(self.context);
			return;
		}

		if(bApply)
		{
			self.bDidSomething = true;
			//Now get the apply button
			var applyButton = doc.getElementsByName("cbi.apply")[0];
			if(applyButton)
			{
	console.log("WHERE " + __FUNCTION__ + "()  - " + __FILE__ + ":" + __LINE__);
				self.context.targetIFrame.onload=self.changeDHCP4Settings
				self.context.mouseClick(doc, applyButton);
			}
			else
			{
				console.log("Could not find the apply button");
				self.context.cb_failed(self.context);
				return;
			}
		}
		else
		{
			self.context.targetIFrame.onload=null;
			self.context.targetIFrame.src="about:blank";
			self.context.cb_success(self.context, self.routerInfo, self.bDidSomething);
		}
	}


	console.log("OPENWRT ROUTER PLUGIN STARTED");

	//Check the routermodel - do we support it
	
/*	if(self.context.routerModel != "openwrt_router")
	{
		self.context.cb_failed(self.context);
		return;
	}
*/
	//Use the default username and password, unless the user has passed some
	if(!self.context.username) {
		self.context.username = "root";
	}

	if(!self.context.password) {
		self.context.password = "";
	}

	self.loginRetry = false;
	self.startVersionPage();
}

