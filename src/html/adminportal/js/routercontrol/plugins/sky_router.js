var sky_router={};


sky_router.supportedModel=function()
{
	//Check self.modelNumber - self.firmwareVersion may also be available
	var self=this;
	
	if(this.modelNumber == "SR101")
	{
		self.hasIP6=true;
		return true;
	}
	else if(this.modelNumber == "SR102")
	{
		self.hasIP6=true;
		return true;
	}
	return false;
}


sky_router.start=function(context)
{
	var self=this;
	self.context=context;

	self.ip4Value=false;
	self.ip6Value=false;
	self.context.MaxReboots=2;
	self.bDidSomething=false;
	this._start()
}

sky_router.undo=function(context)
{
	var self=this;
	self.context=context;

	self.ip4Value=true;
	self.ip6Value=true;
	self.context.MaxReboots=2;
	self.bDidSomething=false;
	this._start()
}

sky_router._start=function()
{
	var self=this;

	self.routerInfo="Sky Router";

	self.context.cb_status(self.context, self.routerInfo, "Looking for a Sky Router");
	

	//For the sky router
	//We can probably support a couple of different models
	//First do a version check
	//If Auth is required, we will get a 401 and need to request a password
	
	//Navigate to the Router Status page, and pick off the model number (and firmware version...)

	self.checkPassword=function()
	{
		console.log("checking password");
		var login = new XMLHttpRequest();

		login.onreadystatechange = function()
		{
			console.log("ready state for login " + this.readyState + "status = " + this.status);
			if(this.readyState == 4)
            {
                console.log("Ready");
                if(this.status == 401 || this.status == 403)
				{
					console.log("wrong credentials");
					self.context.cb_credentials(self.context, self.routerInfo, false, true);
				}
				else if(this.status == 404)
				{
					console.log("Page not found - wrong router");
					self.context.cb_unsupported(self.context, "Unsupported router");
				}
				else if(this.status == 200)
				{
					console.log("Credentials ok");
					self.startVersionPage();
				}
				else
				{
					console.log("Unexpected status for login " + this.status);
					if(!self.loginRetry)
					{
						console.log("Trying again");
						self.loginRetry = true;
						setTimeout(self.checkPassword, 2);
					}
					else
					{
						console.log("NOT Trying again");
						self.context.cb_unsupported(self.context, "Unsupported router");
					}
				}
			}
		}


		//Get the URL with Basic Auth creds added
		var URL=self.context.makeURL("sky_lan_ip_setup.html", true);
		login.open("GET", URL, true);
		login.send();
	}

	self.pingRouter=function()
	{
		self.context.pingRouter("assets/js/init.js", "sky_router_status.html", self.startVersionPage, true);
	}



	self.waitForReboot=function(msg)
	{
		self.context.rebootCount++;
		self.context.rebootMsg=msg + " (Reboot count: " + self.context.rebootCount + ")";
		console.log("Wait for reboot count is now " + self.context.rebootCount);

		//Let the applicaiton do its thing
		self.context.targetIFrame.onload=self.isRebooted;

		self.context.cb_status(self.context, self.routerInfo, "Pairing with " + self.routerInfo, "Waiting for reboot (" + self.context.rebootCount + "/" + self.context.MaxReboots + ")");
	}

	self.isRebooted=function()
	{
		console.log("Waiting for reboot - " + self.context.targetIFrame.contentWindow.location);
		if(self.context.targetIFrame.contentWindow.location == self.context.makeURL("sky_rebootinfo.html"))
		{
			//We have detected the router is rebooting - now we need to poll it.

			//Just wait 5 secs before changing the page so we can see it reboot for debug purposes
			setTimeout(function()
					{
						self.context.targetIFrame.onload=self.pingRouter;
						self.context.targetIFrame.src="about:blank";
					}, 5000);
		}
	}

	self.checkVersion=function()
	{
		//Get the model and firmware version
		//Now get the Model label.
		var elem = self.context.getElementByText(self.context.targetIFrame.contentWindow.document, "span", "Model");
		if(elem && elem.nextElementSibling)
		{
			//Model number is its sibling span
			if(elem.nextElementSibling.textContent)
				self.modelNumber = elem.nextElementSibling.textContent.trim();
			else
				self.modelNumber = elem.nextElementSibling.innerText;
		}
		elem = self.context.getElementByText(self.context.targetIFrame.contentWindow.document, "span", "Firmware Version");
		if(elem && elem.nextElementSibling)
		{
			//Firmware version is its sibling span
			if(elem.nextElementSibling.textContent)
				self.firmwareVersion = elem.nextElementSibling.textContent.trim();
			else
				self.firmwareVersion = elem.nextElementSibling.innerText;
		}

		self.routerInfo = "Sky Router : " + self.modelNumber + " (" + self.firmwareVersion + ")";

		if(self.supportedModel())
		{
			self.context.cb_status(self.context, self.routerInfo, "Pairing with " + self.routerInfo);

			//Router is suported - clear the frame (required to remove previous overriden functions)
			self.context.targetIFrame.onload=self.startDHCP4Settings; //this will kick off the DHCP IP4 setting
			self.context.targetIFrame.src="about:blank";
		}
		else
		{
			//Unsupported?
			self.context.cb_unsupported(self.context, "Unsupported model " + self.routerInfo);
		}
	}

	self.startVersionPage=function()
	{
		if(self.context.rebootCount > self.context.MaxReboots)
		{
			self.context.cb_failed(self.context, "Too many reboots " + self.context.rebootCount);
			return;
		}

		top.helpframe={};
		top.helpframe.location={};
		top.helpframe.location.href="";

		self.context.targetIFrame.onload=self.checkVersion;

		//Start the load
		//Pass the credential on the first call only - rely on the browser to store them
		//This avoids an issue where Chrome refuses to send Auth header on the form POST, if the credentials are in the URL
		var URL=self.context.makeURL("sky_router_status.html", true);
		self.context.targetIFrame.src=URL;
	}

	self.startDHCP4Settings=function()
	{
		top.helpframe={};
		top.helpframe.location={};
		top.helpframe.location.href="";

		self.context.cb_status(self.context, self.routerInfo, "Pairing with " + self.routerInfo, "Phase 1 complete");

		self.context.targetIFrame.onload=self.changeDHCP4Settings

		//Start the load
		var URL=self.context.makeURL("sky_lan_ip_setup.html");
		self.context.targetIFrame.src=URL;
	}

	self.startDHCP6Settings=function()
	{
		top.helpframe={};
		top.helpframe.location={};
		top.helpframe.location.href="";

		self.context.cb_status(self.context, self.routerInfo, "Pairing with " + self.routerInfo, "Phase 2 complete");

		self.context.targetIFrame.onload=self.changeDHCP6Settings

		//Start the load
		var URL=self.context.makeURL("sky_lan_ip_setup.html");
		self.context.targetIFrame.src=URL;
	}

	self.changeDHCP4Settings=function()
	{
		var doc=self.context.targetIFrame.contentWindow.document;
		var ip4 = doc.getElementsByName('dhcp_server')[0];
		var bApply = false;

		//Check we have something to do
		if(ip4 !== undefined)
		{
			if(ip4.checked != self.ip4Value)
			{
				console.log("Setting DHCP4 to " + self.ip4Value);
				bApply = true;
				ip4.checked = self.ip4Value;
			}
			else
			{
				console.log("DHCP4 nothing to do");
			}
		}
		else
		{
			console.log("Could not find the ip4 DHCP checkbox");
			self.context.cb_failed(self.context);
			return;
		}

		if(bApply)
		{
			self.bDidSomething = true;
			//Now get the apply button
			var elem = self.context.getElementByText(doc, "span", "Apply");
			if(elem && elem.parentElement)
			{
				//Disable the confirm dialog for applying settings
				self.context.targetIFrame.contentWindow.confirm=function(){console.log("my confirm"); return true;}
				self.waitForReboot("Applied DHCP4 Settings");
				self.context.mouseClick(doc, elem.parentElement);
			}
			else
			{
				console.log("Could not find the apply button");
				self.context.cb_failed(self.context);
				return;
			}
		}
		else if(self.hasIP6)
		{
			console.log("Nothing to apply - starting dhcp6 settings");
			self.context.targetIFrame.onload=self.startDHCP6Settings; //this will kick off the DHCP IP6 setting
			self.context.targetIFrame.src="about:blank";
		}
		else
		{
			self.context.targetIFrame.onload=null;
			self.context.targetIFrame.src="about:blank";
			self.context.cb_success(self.context, self.routerInfo, self.bDidSomething);
		}
	}

	self.changeDHCP6Settings=function()
	{
		var doc=self.context.targetIFrame.contentWindow.document;
		var ip6 = doc.getElementById('id_enblDhcp6s');
		var bApply = false;

		//Check we have something to do
		if(ip6 !== undefined)
		{
			if(ip6.checked != self.ip6Value)
			{
				console.log("Setting DHCP6 to " + self.ip6Value);
				bApply = true;
				ip6.value = (self.ip6Value ? 1 : 0);
				ip6.checked = self.ip6Value;
			}
			else
			{
				console.log("DHCP6 nothing to do");
			}
		}
		else
		{
			console.log("Could not find the ip6 DHCP checkbox");
			self.context.cb_failed(self.context);
			return;
		}

		if(bApply)
		{
			self.bDidSomething = true;
			//Now get the apply button
			//For this section - we need to do a form submit
			var button = doc.getElementById("a_frm_lan_apply");
			if(button)
			{
				console.log("Clicking apply");
				//Disable the confirm dialog for applying settings
				self.context.targetIFrame.contentWindow.confirm=function(){console.log("my confirm"); return true;}
				self.waitForReboot("Applied DHCP6 Settings");
				self.context.mouseClick(doc, button);
			}
			else
			{
				console.log("Could not find the apply button");
				self.context.cb_failed(self.context);
				return;
			}
		}
		else
		{
			self.context.targetIFrame.onload=null;
			self.context.targetIFrame.src="about:blank";
			self.context.cb_success(self.context, self.routerInfo, self.bDidSomething);
		}
	}

	console.log("SKY ROUTER PLUGIN STARTED");

	//Check the routermodel - do we support it
	
	if(self.context.routerModel != "sky_router")
	{
		self.context.cb_failed(self.context);
		return;
	}

	//Use the default username and password, unless the user has passed some
	if(!self.context.username) {
		self.context.username = "admin";
	}

	if(!self.context.password) {
		self.context.password = "sky";
	}

	self.loginRetry = false;
	self.checkPassword();
}

