var routercontrol={};

window.__defineGetter__('__LINE__', function() {
   return (new Error).stack.split('\n')[2].match(/.*:([0-9]+):[0-9]+/)[1];
});

window.__defineGetter__('__FUNCTION__', function() {
   return (new Error).stack.split('\n')[2].match(/at (.*)\s/)[1];
});

window.__defineGetter__('__FILE__', function() {
  return (new Error).stack.split('\n')[2].match(/http[s]?:\/\/.*\/(.*):.*:/)[1];
});

routercontrol.parentAppWindow=null;

//Once the routercontrol window is created, we cant start until we have receiveid a message from our parent
//since it tells us who to notify on significant events.
//The parent sends us a notify event every 5 secs, incase we missed one
routercontrol.receiveMessage=function(event)
{
	var msg=event.data;

	if(msg.msg == "parentNotify")
	{
		console.log("Got parentNotify message");

		if(routercontrol.parentAppWindow == null)
		{
			routercontrol.parentAppWindow = event.source;
			window.moveTo(0, 0);
		    window.blur();

			routercontrol.startRouterControl();
		}
		else
		{
			routercontrol.parentAppWindow = event.source;
		}
		msg={ msg:"childack"};
		routercontrol.parentAppWindow.postMessage(msg, "*");
	}	
}


//Get stuff from the query string
routercontrol.getParameterByName=function(name, url) 
{
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

routercontrol.mouseClick=function(doc, el)
{
	//debugger;
    if (el.fireEvent) 
    {
        el.fireEvent('onclick');
    } 
    else 
    {

		var evObj = new MouseEvent('click', {
		    'view': window,
		    'bubbles': true,
		    'cancelable': true
		 });
        el.dispatchEvent(evObj);
    }
}
routercontrol.eventFire=function(doc, el, etype)
{
    if (el.fireEvent) 
    {
        el.fireEvent('on' + etype);
    } 
    else 
    {
        var evObj = doc.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    }
}

routercontrol.getElementByText=function(doc, tag, text)
{
    var tags = doc.getElementsByTagName(tag);
    var found;

    for (var i = 0; i < tags.length; i++) 
    {
      if (tags[i].textContent == text) 
      {
          found = tags[i];
          break;
      }
    }
    return found;
}


routercontrol.loadPlugin=function(plugin, callback)
{
    // Adding the script tag to the head
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "/blxadmin/js/routercontrol/plugins/" + plugin + ".js";

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

    // Fire the loading
    head.appendChild(script);
}


routercontrol.plugins = [];
routercontrol.pluginIndex = 0;

routercontrol.startRouterControl=function()
{
	this.plugins = this.getParameterByName("plugins").split(',');
	this.pluginIndex=-1;
	this.continuePluginLoad();
}

routercontrol.continuePluginLoad=function()
{
	var self = this;
	self.pluginIndex++;
	if(!self.plugins[self.pluginIndex])
	{
		self.onPluginNoneSupported();
		return;
	}
	self.loadPlugin(self.plugins[self.pluginIndex], function() { self.onPluginLoad(self.plugins[self.pluginIndex]);} );
}

routercontrol.onPluginLoad=function(plugin)
{
	//Test whether this is the right plugin
//	console.log("Plugin " + plugin + " loaded");
//
//	Start the plugin, with a load of parameters

	var routerFrame = document.getElementById('routerframe');



	var self = this;

//var test=window.open("www.google.com", "_blank");
//debugger;
//console.log(test);

	//Always create a new plugin context
	self.pluginContext=
	{
		plugin: plugin,
		pluginIndex: self.pluginIndex,
		routercontrol: self,
		href:window.location.href,
		routerUI: window.location.host,
		routerIP: self.getParameterByName("routerip"),
		routerModel: self.getParameterByName("routermodel"),
		username: self.getParameterByName("username"),
		password: self.getParameterByName("password"),
		targetIFrame: routerFrame,
		rebootCount:0,
		MaxReboots:0,
		rebootMsg:"",


		//Utils
		getElementByText:self.getElementByText,
		eventFire:self.eventFire,
		mouseClick:self.mouseClick,
		getParameterByName:self.getParameterByName, //Can be used to get all the URL parameters to the routercontrol frame (shouldn't be required)


		//Callbacks
		cb_success:self.onPluginSuccess, //Succeeded router is now sorted
		cb_unsupported:self.onPluginUnsupported,  //Will try next plugin
		cb_failed:self.onPluginFail,  //Will try next plugin
		cb_credentials:self.onPluginNeedCredentials, //Will ask the user for credentials
		cb_status:self.onPluginStatus, //Send a message to the status-tray in the UI.

		//END
		empty:undefined
	}

	//Some more utils
	self.pluginContext.makeURL=function(page, creds)
	{
		var self = this;
		return "http://" + 
					(creds ? self.username + ":" + self.password + "@" : "") +
					self.routerUI + "/" + page;
	}

	self.pluginContext.pingRouter=function(nonAuthPage, authPage, callback, bAddTime)
	{
		var context = this;
		console.log("starting ping");

		//Box is rebooting - we need to wait for it to come back
		var ping = new XMLHttpRequest();
		ping.timeout=5000; //Wait no more than 5 seconds for a response
		ping.hasBeenDown = false;
		ping.waitFor200 = false;
		ping.waitForAuth = false;
		ping.startTime = Date.now();

		ping.onreadystatechange = function()
		{
			console.log("ready state for ping " + ping.readyState + "status = " + ping.status);

			if(ping.readyState == 4)
			{
				console.log("Ready");
				if(ping.status == 404 && ping.hasBeenDown)
				{
					result = ping.getAllResponseHeaders();
					console.log("Got a 404 ");
					ping.hasBeenDown=false;
					ping.waitFor200=true;
				}
				else if(ping.status == 200 && ping.waitFor200)
				{
					console.log("Got 200 - Box is UP");
					ping.waitFor200=false;
					ping.waitForAuth=true;
				}
				else if(ping.status == 200 && ping.waitForAuth)
				{
					//All up and running now
					setTimeout(function()
							{
								context.targetIFrame.onload=callback;
								context.targetIFrame.src="about:blank";
							});
					return;
				}
				else if(ping.status == 500 || ping.status == 0)
				{
					ping.hasBeenDown = true;

				}

				var strTime="";
				if(bAddTime)
				{
					strTime="?time=" + Date.now();
				}

				if(ping.waitFor200)
				{
					ping.pingURL=context.makeURL(nonAuthPage + strTime);
				}
				else if(ping.waitForAuth)
				{
					//Request with credentials
					ping.pingURL=context.makeURL(authPage+strTime, true);
				}
				else
				{
					ping.pingURL=context.makeURL("garbage?time=" + Date.now());
				}

				//Wait 2 mins for a reboot
				var waitMS = (2 * 60) * 1000;
				if((Date.now() - ping.startTime) > waitMS)
				{
					context.cb_failed(context, "Failed waiting for reboot");
				}
				else
				{
					console.log("Waiting 5 more seconds. Total wait " + (Date.now() - ping.startTime) + " ms");
					context.targetIFrame.contentWindow.document.body.innerHTML = "<p><div>Waiting 5 more seconds. Total wait " + 
													(Date.now()-ping.startTime)/1000 + " s</div></p>" + 
													"<p><div>Status: " + ping.status + "</div></p>" +
													"<p><div>Status: " + context.rebootMsg + "</div></p>" 
					setTimeout(function()
							{
								ping.open("GET", ping.pingURL, true);
								ping.send();
							}, 5000);
				}
			}
			else
			{
				//Dont care
			}
		}

		ping.pingURL=context.makeURL("garbage?time=" + Date.now());
		ping.open("GET", ping.pingURL, true);
		ping.send();
	}


	self.pluginContext.targetIFrame.contentWindow = self.pluginContext.targetIFrame;

	var strAction = self.getParameterByName("action");

	var f = new Function('obj', plugin + '.' + strAction + '(obj);');
	f(self.pluginContext);
}

routercontrol.onPluginUnsupported=function(context, msg)
{
	var self=context.routercontrol;
	console.log("Plugin " + context.plugin + " not supported");
	var msg={ msg:"unsupported", routermodel:context.routermodel, help:msg, plugin:context.plugin };
	if(!routercontrol.getParameterByName("standalone"))
	{
		routercontrol.parentAppWindow.postMessage(msg, "*");
	}
	self.continuePluginLoad();
}

routercontrol.onPluginNoneSupported=function()
{
	var self=this;
	var routerModel = self.getParameterByName("routermodel");

	console.log("Plugin not found for " + routerModel + " not supported");
	var msg={ msg:"noplugin", routermodel:routerModel};
	if(!routercontrol.getParameterByName("standalone"))
	{
		routercontrol.parentAppWindow.postMessage(msg, "*");
	}
}

routercontrol.onPluginFail=function(context, mesg)
{
	var self=context.routercontrol;
	console.log("Plugin " + context.plugin + " failed - error in plugin");
	var msg={ msg:"error", routermodel:context.routermodel, help:mesg };

	if(routercontrol.getParameterByName("standalone"))
	{
		alert("STANDALONE MODE ALERT: Plugin failed: Would normally close window " + JSON.stringify(msg));
	}
	else
	{
		routercontrol.parentAppWindow.postMessage(msg, "*");
		if(!routercontrol.getParameterByName("noclose"))
		{
			setTimeout(window.close);
		}
	}
}


routercontrol.onPluginSuccess=function(context, routerinfo, didsomething)
{
	console.log("Plugin " + context.plugin + " succeeded");
	var self=context.routercontrol;
	var msg={ msg:"success", routermodel:context.routermodel, help:msg, routerinfo:routerinfo, didsomething:didsomething };
	if(routercontrol.getParameterByName("standalone"))
	{
		alert("STANDALONE MODE ALERT: Plugin success: Would normally close window " + JSON.stringify(msg));
	}
	else
	{
		routercontrol.parentAppWindow.postMessage(msg, "*");
		if(!routercontrol.getParameterByName("noclose"))
		{
			setTimeout(window.close);
		}
	}
}


routercontrol.onPluginNeedCredentials=function(context, routerinfo, username, password)
{
	console.log("Plugin " + context.plugin + " needs password");
	var msg={ msg:"credentials", password:password, username:username, routermodel:context.routermodel, routerinfo:routerinfo};
	if(routercontrol.getParameterByName("standalone"))
	{
		alert("STANDALONE MODE ALERT: Need Credentials: Would normally close window " + JSON.stringify(msg));
	}
	else
	{
		routercontrol.parentAppWindow.postMessage(msg, "*");
		if(!routercontrol.getParameterByName("noclose"))
		{
			setTimeout(window.close);
		}
	}
}

routercontrol.onPluginStatus=function(context, routerinfo, msgtitle, mesg)
{
	var msg={ msg:"status", title:msgtitle, status: mesg, routerinfo:routerinfo};
	if(routercontrol.getParameterByName("standalone"))
	{
		console.log("Plugin Status : " + JSON.stringify(msg));
	}
	else
	{
		routercontrol.parentAppWindow.postMessage(msg, "*");
	}
}


//MAIN
if(routercontrol.getParameterByName("fullscreen"))
{
	var routerFrame = document.getElementById('routerframe');
	routerFrame.width="100%";
	routerFrame.height="100%";
}
if(routercontrol.getParameterByName("standalone"))
{
	routercontrol.parentAppWindow = window;
	routercontrol.startRouterControl();
}
else
{
	//We need to listen to messages from the app
	window.addEventListener("message", routercontrol.receiveMessage, false);
}

