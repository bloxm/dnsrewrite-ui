var bloxmControllers = angular.module('bloxmControllers');

bloxmControllers.controller('devicesCtrl', [ '$scope', '$http', '$interval', '$log', 'configService', 'blxSessionService', '$mdMedia', '$mdDialog',

function ($scope, $http, $interval, $log, configService, sessionService, $mdMedia, $mdDialog) {

    bloxmControllers.addJSONPathHelpers($scope, configService, $log);
    $scope.devices = {};
    $scope.dhcp = {};
    $scope.defaultpolicy = {};
	$scope.configService = configService;
	$scope.sessionService = sessionService;
	$scope.mdMedia = $mdMedia;


	//Take a device thats currently unregistered (from the dhcp dataset), and register it
	//by creating appropriate nodes in the device dataset
	//
	/*INPUT:
	{
      "name": "b0:0b:1e:50:00:07",
      "files": [
        {
          "name": "name",
          "data": [
            "Test device 1"
          ]
        },
        {
          "name": "state",
          "data": [
            "UNREGISTERED"
          ]
        }
      ]
    }
	Expected OUTPUT:
	//In the 'dhcp' dataset, the entry should look like
	{
      "name": "b0:0b:1e:50:00:06",
      "links": [
        {
          "name": "config",
          "targetpath": "\/etc\/dnsrewrite\/devices",
          "targetname": "b0:0b:1e:50:00:07",
          "broken": false
        },
        {
          "name": "state",
          "targetpath": "\/etc\/dnsrewrite\/devices\/b0:0b:1e:50:00:07",
          "targetname": "state",
          "broken": false
        }
      ],
      "files": [
        {
          "name": "name",
          "data": [
            "Test device 1"
          ]
        }
      ]
    }
	//In the 'devices' dataset
	{
      "name": "b0:0b:1e:50:00:07",
      "links": [
        {
          "name": "dnsblock",
          "targetpath": "..\/..\/default-policy",
          "targetname": "Medium",
          "broken": false
        }
      ],
      "files": [
        {
          "name": "name",
          "data": [
            "Test device 1"
          ]
        },
        {
          "name": "state",
          "data": [
            "FILTERED"
          ]
        }
      ]
    }
	*/
	$scope.registerDevice = function(vpUnregisteredDev, initialState, deviceName)
	{
		var MAC = vpUnregisteredDev.ref.name;
		var IP = $scope.get($scope.dhcp, "links[?(@.targetname == '" + vpUnregisteredDev.ref.name + "')].name")
		var vpStateFile = $scope.getvalpath(vpUnregisteredDev, "files[?(@.name == 'state')]");
		var vpDeviceName = $scope.getvalpath(vpUnregisteredDev, "files[?(@.name == 'name')].data[0]");

		if(!deviceName)
		{
			deviceName = vpDeviceName.ref;
		}

		if(!deviceName){ return; }

		//Remove the state file
		var reg = /^\d+/;
		var stateFileIndex = vpStateFile.path.substring(vpStateFile.path.lastIndexOf("[") + 1).match(reg)[0];
		vpUnregisteredDev.ref.files.splice(stateFileIndex, 1);

		//Create the links
		vpUnregisteredDev.ref.links = [
			{
			  "name": "config",
			  "targetpath": "\/etc\/dnsrewrite\/devices",
			  "targetname": MAC,
			  "broken": false
			},
			{
			  "name": "state",
			  "targetpath": "\/etc\/dnsrewrite\/devices\/" + MAC,
			  "targetname": "state",
			  "broken": false
			}
		];

		//Now build the entry in the devices - if we have an entry with the same MAC - lets first remove it (shouldn't happen?)
		var vpEntry = $scope.getvalpath($scope.devices, "children[?(@.name == '" + MAC + "')]", '$scope.devices');

		if(vpEntry)
		{
			var childIndex = vpEntry.path.substring(vpEntry.path.lastIndexOf("[") + 1).match(reg)[0];
			$scope.devices.children.splice(childIndex, 1);
		}
		vpEntry = {
			"name": MAC,
			"links": [
		        {
        		  "name": "dnsblock",
		          "targetpath": "..\/..\/default-policy",
		          "targetname": "Medium",
		          "broken": false
        		}
      		],
			"files": [
        		{
		          "name": "name",
		          "data": [ deviceName ]
		        },
        		{
		          "name": "state",
        		  "data": [ initialState ]
        		}
      		]
		};
		if(!$scope.devices.children)
		{
			$scope.devices.children = [];
		}
		$scope.devices.children.push(vpEntry);
		configService.configChange("devices");
		configService.configChange("dhcp");
	}
	$scope.onSwitchFilter = function(vpRegisteredDev)
	{
		$log.info("onSwitchFilter");
		debugger;
	}
	$scope.setDeviceState = function(vpRegisteredDev, state, deviceName)
	{
		$log.info("setDeviceState");

		var vpEntry = $scope.getvalpath(vpRegisteredDev, "files[?(@.name == 'state')].data[0]");

		if(vpEntry)
		{
			vpEntry.setval($scope, state);
		}

		if(deviceName)
		{
			vpEntry = $scope.getvalpath(vpRegisteredDev, "files[?(@.name == 'name')].data[0]");
			if(vpEntry)
			{
				vpEntry.setval($scope, deviceName);
			}
		}

		configService.configChange("devices");
	}
    $scope.cycleProtection = function(device)
    {
	var stateFile=$scope.getall(device, 'files[?(@.name == \'state\')].data[0]');
	var path='files[?(@.name == \'state\')].data[0]';
	var state= $scope.get(device, path)  ;
	//"$['files'][1]['data'][0]"
	//var state="UNFILTERED";
	switch(state)
	{
	    case 'UNFILTERED':
			$scope.set(device, path, 'FILTERED');
   	    break;
	    case 'FILTERED':
			$scope.set(device, path, 'BLOCKED');
   	    break;
	    case 'BLOCKED':
			$scope.set(device, path, 'UNFILTERED');
   	    break;
	};
    }

	$scope.viewLogs = function(device, deviceName, ev)
	{
		$mdDialog.show({
		  controller: 'logCtrl',
		  //scope:$scope.$new(),
		  templateUrl: 'templates/logview.html',
		  parent: angular.element(document.body),
		  targetEvent: ev,
		  clickOutsideToClose:true,
		  fullscreen: true,
		  resolve: {
			  macAddr : function() {
				  return device;
			  },
			  deviceName : function() {
				  return deviceName;
			  }
		  }
		})
	}

	configService.bind('devices', $scope, true);
	configService.bind('dhcp', $scope, true);

    configService.get('defaultpolicy', true).then( //No need to poll
			function(obj){
				$scope.defaultpolicy = obj;
			});

	$scope.$on("$destroy", function()
	{
        configService.unbind('devices', $scope);
        configService.unbind('dhcp', $scope);
    });

}
]);

