var bloxmControllers = angular.module('bloxmControllers', []);

bloxmControllers.addJSONPathHelpers = function(obj, configService, $log)
{
	obj.configChanged = function(cfgtype){
		configService.configChange(cfgtype);
	}
	obj.pvHashCode = function(s){
  		return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);              
	}
    obj.get = function(o, path)
	{ 
		var respath = jsonPath(o, path, {resultType:"PATH"});
		if(respath)
		{
			var f = new Function('obj', 'return obj' + respath[0].substring(1));
			var ref = f(o);
			return ref;
		}
    };
	obj.getvalpathdbg= function(objectOrResult, path, pathprefix)
	{
		debugger;
		return obj.getvalpath(objectOrResult, path, pathprefix);
	}
	obj.getvalpath= function(objectOrResult, path, pathprefix)
	{
//		$log.info("GETVALPATH - " + JSON.stringify(objectOrResult));
		var result={_BLX_res:true, ref:objectOrResult, path:pathprefix, root:pathprefix};
		var obj={};
		if(objectOrResult.hasOwnProperty("_BLX_res"))
		{
			//Its one of our results
			result.ref = objectOrResult.ref;
			if(pathprefix !== undefined)
			{
				result.path = pathprefix;
			}
			else
			{
				result.path = objectOrResult.path;
			}
			result.root = objectOrResult.root;
		}
		var respath = jsonPath(result.ref, path, {resultType:"PATH"});
		if(respath)
		{
			result._getval = new Function('obj', 'return obj' + respath[0].substring(1));
			result.getval = new Function('obj', 'return obj.' + result.path + respath[0].substring(1));
			result.setval = new Function('obj', 'value', 'obj.' + result.path + respath[0].substring(1) + '=value');
			result.ref = result._getval(result.ref);
			result.path = result.path + respath[0].substring(1);

			return result;
		}
	}
    obj.getparentvalpatharray = function(o, path, parentIndex, pathprefix) 
	{ 
		if(!o || !path)
			return undefined;
		if(pathprefix === undefined)
			pathprefix = "";

		var respaths=jsonPath(o, path, {resultType:"PATH"});
		if(respaths && respaths.length)
		{
			var retarray = [];
			for (var i in respaths)
			{ 
				var parentIdx = parentIndex;
				while(parentIdx-- && respaths[i] && respaths[i].length){
					respaths[i] = respaths[i].substring(0, respaths[i].lastIndexOf("["));
				}
				if(respaths[i])
				{
					respaths[i] = respaths[i].substring(1);
					var f = new Function('obj', 'return obj' + respaths[i]);
					var ref = f(o);
					var newpath=pathprefix + respaths[i];
					retarray.push({_BLX_res:true, 'ref':ref, 'path':newpath, 'root':pathprefix});
				}
			}
			//We want to return the same object every time, unless somethign has changed
			//hash it with our parameters, and store it against ourself in an inert attribute
			var pvHash = 'pvHash' + obj.pvHashCode('' + path + parentIndex, pathprefix);
			var resultSum = configService.createHash(retarray);
			if(o._BLX_pvHashes === undefined)
			{
				o._BLX_pvHashes = {};
			}
			if(o._BLX_pvHashes[pvHash] === undefined || o._BLX_pvHashes[pvHash].sum != resultSum)
			{
				o._BLX_pvHashes[pvHash] = {'sum':resultSum, 'result':retarray};
			}

			return o._BLX_pvHashes[pvHash].result;
		}
    };

	obj.set = function(o, path, val, cfgtype) 
    { //This function take a JSONPath expression, evals it, takes the resultant path, which it then uses to find the 'real' object (via eval)
		//It then set the value, on the real node (hich ensures $scope is updated correctly
		var path=jsonPath(o, path, {resultType:"PATH"})[0];
		path=path.substring(1);

		var f = new Function('obj', 'value', 'obj' + path + '= value');
		f(o, val);
		configService.configChange(cfgtype);
    };
}

bloxmControllers.directive(
{
	require: ['ngModel'],
    'blxBindChild': function ($compile, $log) {
		return {
			scope: {blxModel:'&', blxCallback:'@', blxType:'@', placeholder:'@' },
			transclude:true,
			compile: function(tEl,tAtr){
				return function(scope, iEl, iAtr, ctrl, transclude)
				{
					var res=null;
					if(scope.blxModel){
						res = scope.blxModel();
					}

					transclude(scope.$new(), 
						function(clone) 
						{
							for (i=0; i < clone.length; i++)
							{
								if(clone[i].setAttribute && clone[i].getAttribute)
								{
									
									if(clone[i].localName.startsWith('blx-'))
									{
										var elemName=clone[i].localName.substr(4);
										var html=clone[i].outerHTML;
										html=html.replace(clone[i].localName, elemName);
										clone[i] = angular.element(html)[0];
									}

									if(res && res.path)
									{
										clone[i].setAttribute('ng-model', res.path);
									}
									else
									{
										$log.error("There is no data to model - your valpath expression is probably wrong, or data not initialized");
									}

									if(clone[i].getAttribute('blx-type')){
										clone[i].setAttribute('type', clone[i].getAttribute('blx-type'));
										clone[i].removeAttribute('blx-type');
									}

									if(clone[i].getAttribute('placeholder')){
										clone[i].setAttribute('aria-label', clone[i].getAttribute('placeholder'));
									}

									if(!clone[i].getAttribute('ng-change'))
									{
										if(scope.blxCallback){
											clone[i].setAttribute('ng-change', scope.blxCallback);
										}
										else if(res){
											clone[i].setAttribute('ng-change', 'configChanged("' + res.root + '");');
										}
									}	
									clone[i].onkeydown = function(event)
									{
										if (event.which === 13) 
										{
											event.target.blur();		
										}
									}
									clone[i].onkeypress = clone[i].onkeydown;
								}
							}
							iEl.replaceWith(clone);
							iEl.on("keydown keypress", function(event) 
							{
								if (event.which === 13) 
								{
									event.target.blur();		
								}
							});
							$compile(clone)(scope.$parent);
					    });
				};
			},	
		};
    }
});

var compareTo = function() {
return {
  require: "ngModel",
  scope: {
	otherModelValue: "=compareTo"
  },
  link: function(scope, element, attributes, ngModel) {

	ngModel.$validators.compareTo = function(modelValue) {
	  return modelValue == scope.otherModelValue;
	};

	scope.$watch("otherModelValue", function() {
	  ngModel.$validate();
	});
  }
};
};

bloxmControllers.directive("compareTo", compareTo);


bloxmControllers.directive('focusOn',function($timeout) {
    return {
        restrict : 'A',
        link : function($scope,$element,$attr) {
            $scope.$watch($attr.focusOn,function(_focusVal) {
                $timeout(function() {
                    _focusVal ? $element.focus() :
                        $element.blur();
						$element[0].selectionStart=0;
						$element[0].selectionEnd=$element[0].value.length+1;
                });
            });
        }
    }
});
