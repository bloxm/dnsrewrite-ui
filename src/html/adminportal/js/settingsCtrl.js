var bloxmControllers = angular.module('bloxmControllers');

bloxmControllers.controller('settingsCtrl', [ '$scope', '$http', '$timeout', '$interval', '$log', 'configService', 'blxSessionService', '$mdMedia', '$sce', '$location',

                            function ($scope, $http, $timeout, $interval, $log, configService, sessionService, $mdMedia, $sce, $location)
{
    var self = this;

    $scope.system= {};
    $scope.DNSProvider="";
    $scope.CATProvider="";
    $scope.LocalBlock="";

    bloxmControllers.addJSONPathHelpers($scope, configService, $log);

    self.configService = configService;
	self.sessionService = sessionService;
    $scope.configService = configService;

    $scope.toggleMode=function()
    {
        if(!$scope.system.files)
            return;

        //The router is now paired
        //Set our mode to BASICFILTER
        //update our 'system' config
        if($scope.get($scope.system, "files[?(@.name == 'state')].data[0]") == 'BASICFILTER')
        {
            $scope.set($scope.system, "files[?(@.name == 'state')].data[0]", 'PRISTINE');
        }
        else
        {
            $scope.set($scope.system, "files[?(@.name == 'state')].data[0]", 'BASICFILTER');
        }
        self.configService.save('system', self.sessionService.isLoggedIn()).then(function()
        {
            $timeout(function()
            {
                $scope.$apply();
            })
        });
    }

    $scope.$on("$destroy", function()
    {
    });

    self.configService.get('dnsproviders', self.sessionService.isLoggedIn()).then(
        function(obj)
    {
        $scope.dnsproviders = obj;
    });
    self.configService.get('catproviders', self.sessionService.isLoggedIn()).then(
        function(obj)
    {
        $scope.catproviders = obj;
    });

    self.getSystemConfig=function()
    {
		self.configService.get('system', self.sessionService.isLoggedIn()).then(
			function(obj)
		{
			$scope.system = obj;
			$scope.DNSProvider = $scope.get($scope.system, "files[?(@.name == 'dnsprovider')].data[0]");
			$scope.CATProvider = $scope.get($scope.system, "files[?(@.name == 'catprovider')].data[0]");
			$scope.LocalBlock = $scope.get($scope.system, "files[?(@.name == 'localblock')].data[0]");
		});
    }

	self.getSystemConfig();

    $scope.setProviders=function()
    {
        $scope.set($scope.system, "files[?(@.name == 'dnsprovider')].data[0]", $scope.DNSProvider);
        $scope.set($scope.system, "files[?(@.name == 'catprovider')].data[0]", $scope.CATProvider);
        $scope.set($scope.system, "files[?(@.name == 'localblock')].data[0]", $scope.LocalBlock);
        self.configService.save('system', self.sessionService.isLoggedIn()).then(function(obj)
        {
            $timeout(function()
            {
				self.getSystemConfig();
                $scope.$apply();
            });
        });
    }
}
                                            ]);

