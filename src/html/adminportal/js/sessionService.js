var blxSession = angular.module("blxSession", [ "dgAuth", "ngIdle" ]);

blxSession.config([ "KeepaliveProvider", "IdleProvider", "dgAuthServiceProvider", "$httpProvider", "$locationProvider", 
function(KeepaliveProvider, IdleProvider, dgAuthServiceProvider, $httpProvider, $locationProvider) {
    IdleProvider.keepalive(false);
    IdleProvider.idle(900);
    IdleProvider.timeout(30);
    KeepaliveProvider.interval(30);
    self.LoginURL = "/blxadmin/cgi-bin/private/config.cgi?config=login";
    self.LogoutURL = "/blxadmin/cgi-bin/private/config.cgi?config=logout";
    dgAuthServiceProvider.setHeader("BLX-Authenticate");
    dgAuthServiceProvider.setConfig({
        login: {
            method: "GET",
            url: self.LoginURL
        },
        logout: {
            method: "GET",
            url: self.LogoutURL
        }
    });
    $httpProvider.interceptors.push([ "$log", "$q", "$injector", function($log, $q, $injector) {
        return {
            responseError: function(rejection) {
                if (rejection.status === 403) {
                    $log.info("Seen a 403 ");
                    var sessionService = $injector.get("blxSessionService");
                    sessionService.loginMessage = rejection.headers("BLXInfo");
                    sessionService.Logout();
                }
                return $q.reject(rejection);
            },
            request: function(request) {
                var sessionService = $injector.get("blxSessionService");
                if (sessionService) {
                    request.headers.Blx_Session = sessionService.getSessionKey();
                } else {
                    $log.error("Failed to get session service");
                }
                return request || $q.when(request);
            }
        };
    } ]);
    dgAuthServiceProvider.callbacks.login.push([ "blxSessionService", "$location", "$log", "$timeout", "$route", function(blxSessionService, $location, $log, $timeout, $route) {
        return {
            successful: function(response) {
                $log.info("login:successful");
                blxSessionService.authComplete = true;
                blxSessionService.onUIAlive();
            },
            error: function(response) {
                $log.info("login:error");
				if(self.username)
				{
	                blxSessionService.loginMessage = "Login failed. Username or password incorrect";
				}
                blxSessionService._Logout();
                blxSessionService.authComplete = false;
				$location.path("/welcome").search("returnTo", $location.path());
            },
            required: function(response) {
                if (blxSessionService.transferCredentials()) {} else {
                    $log.info("login:required");
                    blxSessionService._Logout();
                    blxSessionService.authComplete = false;
                    $location.path("/welcome").search("returnTo", $location.path());
                }
            },
            limit: function(response) {
                $log.info("login:limit");
            }
        };
    } ]);
    dgAuthServiceProvider.callbacks.logout.push([ "blxSessionService", "$log", function(blxSessionService, $log) {
        return {
            successful: function(response) {
                $log.info("logout:successful");
                blxSessionService.authComplete = false;
            },
            error: function(response) {
                $log.info("logout:error");
                blxSessionService.authComplete = false;
            }
        };
    } ]);
} ]);

blxSession.provider("blxSessionService", function BlxSessionServiceProvider() {
    function blxSessionService($http, dgAuthService, Keepalive, Idle, sessionKeyStore, $location) {
        var self = this;
        self.loggedIn = false;
        self.loginMessage = "";
        self.logoutString = "Logout";
        self.keyStore = sessionKeyStore.$default({
            currentSessionKey: ""
        });
        self.randString = function(x) {
            var s = "";
            while (s.length < x && x > 0) {
                var r = Math.random();
                s += r < .1 ? Math.floor(r * 100) : String.fromCharCode(Math.floor(r * 26) + (r > .5 ? 97 : 65));
            }
            return s;
        };
        self.generateSessionKey = function() {
            self.keyStore.currentSessionKey = self.randString(32);
            return self.keyStore.currentSessionKey;
        };
        self.getSessionKey = function() {
            if (!self.keyStore.currentSessionKey) return self.generateSessionKey();
            return self.keyStore.currentSessionKey;
        };
        self.isLoggedIn = function() {
            return self.loggedIn;
        };
        self.getLoginMessage = function() {
            return self.loginMessage;
        };
        self.Logout = function() {
            self.onUIDead();
        };
        self.bStarted = false;
        self.Start = function() {
            if (!self.bStarted) {
                dgAuthService.start();
                self.bStarted = true;
            }
        };
        self._Logout = function() {
            self.loggedIn = false;
            self.currentSessionKey = "";
            Keepalive.stop();
            Idle.unwatch();
        };
        self.Login = function() {
            if (!self.bStarted) {
                self.Start();
            } else {
                dgAuthService.signin();
            }
        };
        self.setCredentials = function(username, password) {
            self.username = username;
            self.password = password;
            dgAuthService.setCredentials(self.username, self.password);
        };
        self.transferCredentials = function() {
            if (self.username) {
                dgAuthService.setCredentials(self.username, self.password);
                self.username = undefined;
                self.password = undefined;
                return true;
            }
            return false;
        };
        self.onUIAlive = function() {
            self.logoutString = "Logout";
            if (self.authComplete) {
                $http({
                    method: "POST",
                    url: "/blxadmin/cgi-bin/private/config.cgi?config=session",
                    data: {
                        name: "session",
                        files: [ {
                            name: ".key",
                            data: [ self.getSessionKey() ]
                        } ]
                    }
                }).then(function() {
                    if (!self.loggedIn) {
                        self.loggedIn = true;
                        self.loginMessage = "";
                        Keepalive.start();
                        Idle.watch();
                        var search = $location.search();
                        if (search && search.returnTo) {
                            $location.path("/" + search.returnTo);
                        }
                    }
                }, function() {});
            }
        };
        self.onUILogoutCountdown = function(countdown) {
            self.logoutString = "Logout in " + countdown + "secs";
        };
        self.onUILogoutCountdownCancel = function() {
            self.logoutString = "Logout";
        };
        self.onUIDead = function() {
            if (self.loggedIn) {
                $http({
                    method: "POST",
                    url: "/blxadmin/cgi-bin/private/config.cgi?config=session",
                    data: {
                        name: "session",
                        files: [ {
                            name: ".key",
                            data: []
                        } ]
                    }
                }).then(function() {
                    self._Logout();
                    return dgAuthService.signout();
                }, function() {
                    self._Logout();
                    return dgAuthService.signout();
                });
            } else if (self.authComplete) {
                self._Logout();
                return dgAuthService.signout();
            }
        };
    }
    this.$get = [ "$http", "dgAuthService", "Keepalive", "Idle", "$localStorage", "$location", function($http, dgAuthService, Keepalive, Idle, sessionKeyStore, $location) {
        return new blxSessionService($http, dgAuthService, Keepalive, Idle, sessionKeyStore, $location);
    } ];
});
