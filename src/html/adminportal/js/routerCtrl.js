var bloxmControllers = angular.module('bloxmControllers');

bloxmControllers.controller('routerCtrl', [ '$scope', '$http', '$interval', '$timeout', '$log', 'configService', 'blxSessionService', '$mdMedia', '$sce', '$location',

function ($scope, $http, $interval, $timeout, $log, configService, sessionService, $mdMedia, $sce, $location) 
{
	var self = this;

    bloxmControllers.addJSONPathHelpers($scope, configService, $log);
    $scope.laninfo = {};
	$scope.loaded = false;
	$scope.noSleep = new NoSleep();

	self.configService = configService;
	self.sessionService = sessionService;

	$scope.undoRouterControl=false;
	$scope.username="";
	$scope.password="";

	$scope.bDebug=false;
	if($location.path() == '/routerdebug')
	{
		$scope.bDebug = true;
	}

    $scope.setBasicFilterMode=function() 
	{
		//The router is now paired
		//Set our mode to BASICFILTER
		//update our 'system' config
		self.configService.get('system', self.sessionService.isLoggedIn()).then(function()
			{
				if($scope.get($scope.system, "files[?(@.name == 'state')].data[0]") == 'PRISTINE')
				{
					$scope.set($scope.system, "files[?(@.name == 'substate')].data[0]", ''); 
					$scope.set($scope.system, "files[?(@.name == 'state')].data[0]", 'BASICFILTER'); 
					$scope.set($scope.system, "files[?(@.name == 'services')].data[0]", 'NEEDRESTART'); 
					self.configService.save('system', self.sessionService.isLoggedIn()).then(function()
						{
							$timeout(function()
									{
										$scope.$apply();
										$scope.noSleep.disable();
									})
						});
				}
				else
				{
					$timeout(function()
						{
							$scope.noSleep.disable();
						})
				}
			});
    }

	$scope.bPairing = false;

    $scope.setPairingSubstate=function(bPairing) 
	{
		if($scope.bPairing == bPairing)
			return;

		if(bPairing)
		{
			if ("ontouchstart" in document.documentElement)
			{
				$scope.noSleep.enable();
			}
		}
		else
		{
			$scope.noSleep.disable();
		}
		//The router is now paired
		//Set our mode to BASICFILTER
		//update our 'system' config
		self.configService.get('system', self.sessionService.isLoggedIn()).then(function()
			{
				$scope.set($scope.system, "files[?(@.name == 'substate')].data[0]", bPairing ? 'PAIRING' : ''); 
				self.configService.save('system', self.sessionService.isLoggedIn()).then(function()
					{
					});
			});
    }

	$scope.routerIPValidPattern="^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
	$scope.ipValidPattern="/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/";
	$scope.rnd=true;

	$scope.ipAddress="";
	$scope.ipRouter="";
	$scope.ipSubnet="";
	$scope.ipDHCPStart="";
	$scope.ipDHCPEnd="";

	$scope.genApplianceIPValidator=function()
	{
		//ipRouter has been entered - if it is valid, suggest an ApplianceIP
		if(!$scope.ipRouter || !$scope.ipRouter.length)
			return;

		var parts = $scope.ipRouter.split('.');
		if(parts.length == 4)
		{
			$scope.ipValidPattern="^" + parts[0] + "\\." + parts[1] + "\\." + parts[2] + "\\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
		}
	}

	$scope.suggestApplianceIP=function()
	{
		//ipRouter has been entered - if it is valid, suggest an ApplianceIP
		if(!$scope.ipRouter || !$scope.ipRouter.length)
			return;

		var parts = $scope.ipRouter.split('.');
		if(parts.length == 4)
		{
			$scope.ipAddress=parts[0] + "." + parts[1] + "." + parts[2] + ".";

			if(parts[3] < 128)
			{
				$scope.ipAddress+="224";
			}
			else
			{
				$scope.ipAddress+="1";
			}
			$timeout(function(){
				$scope.$apply()
			});
		}
	}

	$scope.setManualPairing=function()
	{
		//Address needs to be assigned to the laninfo gateway
		$scope.set($scope.laninfo, "files[?(@.name == 'gateway')].data[0]", $scope.ipRouter);
		$scope.set($scope.laninfo, "files[?(@.name == 'applianceip')].data[0]", $scope.ipAddress);
        
        self.configService.save('laninfo', self.sessionService.isLoggedIn()).then(function()
        {
			$scope.setBasicFilterMode();
        });
	}
	//MAIN function to start the pairing process
	$scope.startRouterControl=function()
	{
		if(window.location.host != $scope.routerPortal)
		{
			$log.info("Redirecting to routercontrol site");
			if($scope.bDebug)
			{
				window.location = "http://" + $scope.routerPortal + "/blxadmin/#/routerdebug";
			}
			else
			{
				window.location = "http://" + $scope.routerPortal + "/blxadmin/#/routernow";
			}
			return;
		}

		var actionStr="start";
		if($scope.undoRouterControl)
		{
			actionStr="undo";
		}
		if($scope.bRunning)
		{
			return;
		}

		$scope.bSuccess=false;
		$scope.bFailed=false;
		$scope.bRunning=true;
		$scope.bNoPlugin=false;
		$scope.bUsername=false;
		$scope.bPassword=false;

		//Add time  so the IFRAME updates
		var d = new Date;
		var queryArgs = "&action=" + actionStr + 
						"&routerip=" + $scope.routerIP + 
						"&routermodel=" + $scope.routerModel + 
						"&plugins=" + $scope.plugins + 
						"&timenow=" + d.getTime() +
						"&username=" + $scope.username +
						"&password=" + $scope.password;

 	    if($scope.bDebug)
	    {
			queryArgs +="&fullscreen=1&noclose=1";
		}

		var URL="http://" + $scope.routerPortal + "/blxadmin/routercontrol.html?" + queryArgs;
		var nWidth=1;
		var nHeight=1;

		if($scope.bDebug)
		{
			nWidth=1024;
			nHeight=768;
		}

		$scope.setPairingSubstate(true);

		$scope.bUsePopup=false;

		if($scope.bUsePopup)
		{
			$scope.routerControlWindow=null;
			if($scope.routerControlWindow)
			{
				$scope.routerControlWindow.close();
			}
			$scope.routerControlWindow = $scope.launchPopup(URL, 'routercontrol', nWidth, nHeight, 'scrollbars=yes,resizable=yes,menubar=no,toolbar=no,location=no');
		}
		else
		{
			var iframe = window.document.getElementById("routercontrol");
			$scope.routerControlWindow = iframe.contentWindow;
			$scope.router_portal_url = $sce.trustAsResourceUrl(URL)
		}

		//We need to listen to messages from the routercontrol frame/window
		window.addEventListener("message", $scope.receiveMessage, false);

		//Refresh this token every so often
		$scope.routerControlWindow.postMessage("parentNotify", "*");

		$scope.parentNotifyTimer = $interval(function()
				{
					msg={ msg:"parentNotify"};
					$scope.routerControlWindow.postMessage(msg, "*");
				}, 500, 0);
	}
	$scope.launchPopup=function(URL, name, width, height, style) 
	{
		var str = "height=" + height + ",innerHeight=" + height;
		str += ",width=" + width + ",innerWidth=" + width;  
		return window.open(URL, name, str + "," + style);
	}
	$scope.$on("$destroy", function()
	{
		$scope.setPairingSubstate(false);
        configService.unbind('laninfo', $scope);
//        configService.unbind('dhcp', $scope);
    });

	$scope.bUsername=false;
	$scope.bPassword=false;
	$scope.bSuccess=false;
	$scope.bFailed=false;
	$scope.bRunning=false;
	$scope.bNoPlugin=false;
	$scope.hidePassword=true;
	$scope.receiveMessage=function(event)
	{
		var msg=event.data;

		$log.info("Received message " + msg.msg);

		if(msg.msg == "childack")
		{
			$interval.cancel($scope.parentNotifyTimer);
			window.focus();
		}
		else if(msg.msg == "unsupported")
		{
			//nothing to do - this is for info
			$log.info("'Unsupported' from plugin :" + msg.plugin + ". Current router ID :" + msg.routermodel + "." + msg.help);
		}
		else if(msg.msg == "error")
		{
			$scope.bFailed = true;
			$scope.bRunning = false;
			$scope.infoMessage = msg.help;
			$scope.debugMessage = JSON.stringify(msg);
			$scope.setPairingSubstate(false);
		}
		else if(msg.msg == "success")
		{
			$scope.bRunning = false;
			$scope.bSuccess = true;
			$scope.bDidSomething=msg.didsomething;

			if(!$scope.bDebug)
			{
				$log.info("Setting basic filter mode");
				$scope.setBasicFilterMode();
			}
			else
			{
				$scope.setPairingSubstate(false);
			}
		}
		else if(msg.msg == "credentials")
		{
			$scope.bRunning = false;
			$scope.bPassword = msg.password;
			$scope.bUsername = msg.username;
			$scope.username="";
			$scope.password="";
			$scope.setPairingSubstate(false);
		}
		else if(msg.msg == "status")
		{
			$scope.pairingStatusTitle = msg.title;
			$scope.pairingStatusText = msg.status;

		}
		else if(msg.msg == "noplugin")
		{
			$scope.bRunning = false;
			$scope.bNoPlugin = true;
			$scope.infoMessage = msg.routermodel;
			$scope.setPairingSubstate(false);
		}
		$scope.$apply();
	}


	$scope.onLanInfo=function(cfgtype)
	{
		self.configService.get('routerplugins', self.sessionService.isLoggedIn()).then(function(obj)
		{
			$scope.routerplugins = obj;
			//Get the information we need to pass to the routercontroller
			$scope.ipRouter = $scope.routerIP = $scope.get($scope.laninfo, "files[?(@.name == 'routerip')].data[0]");
			$scope.ipAddress = $scope.get($scope.laninfo, "files[?(@.name == 'applianceip')].data[0]");
			var dhcpFail = $scope.get($scope.laninfo, "files[?(@.name == 'dhcpfail')]");
			if(dhcpFail && dhcpFail.data && dhcpFail.data.length && dhcpFail.data[0] == "1")
			{
				$scope.bDHCPFail = true;
			}
			else
			{
				$scope.bDHCPFail = false;
			}

			$scope.genApplianceIPValidator();
			$scope.routerPortal =  $scope.get($scope.laninfo, "files[?(@.name == 'routerportal')].data[0]");
			$scope.routerModel =  $scope.get($scope.laninfo, "files[?(@.name == 'routermodel')].data[0]");

			//testing
			$scope.routerModel="sky_router";	

			var files = $scope.routerplugins.files;
			$scope.plugins = "";

			if($scope.routerplugins.files)
			{
				for(var i=0; i < $scope.routerplugins.files.length; ++i)
				{
					$scope.plugins += $scope.routerplugins.files[i].name.substring(0, $scope.routerplugins.files[i].name.lastIndexOf(".")) + ",";
				}
			}
			/*links?
			for(var i=0; i < $scope.routerplugins.links.length; ++i)
			{
				$scope.plugins += $scope.routerplugins.links[i].name + ",";
			}*/

			$scope.loaded = true;

			//Check if we should start the process straight away
			if($location.path() == '/routernow')
			{
				$scope.startRouterControl();
			}
		});
	}
	//
	//MAIN
	configService.bind('laninfo', $scope, self.sessionService.isLoggedIn(), $scope.onLanInfo);
}
]);

