var bloxmControllers = angular.module('bloxmControllers');

bloxmControllers.controller('welcomeCtrl', [ '$scope', '$log', '$mdDialog', '$mdMedia', 'configService', 'blxSessionService', 'md5',

function ($scope, $log, $mdDialog, $mdMedia, configService, blxSessionService, md5) {

    bloxmControllers.addJSONPathHelpers($scope, configService, $log);
	$scope.system={};
	$scope.password={};
	$scope.hidePassword=true;

	var self=this;
	self.md5 = md5;
	self.configService = configService;
	self.blxSessionService = blxSessionService;
	$scope.blxSessionService = blxSessionService;


	//Parameters for createparent
	$scope.cp={ hidePassword: true, realm:'private' };
	$scope.initLoginParam=function(){
		$scope.loginParam={ hidePassword: true };
	};
	$scope.initLoginParam();

	$scope.showRegisterParentDialog = function(ev) {
		self.configService.get('password').then( //No need to poll
							function(obj){
								$scope.password = obj;
							});
		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
		$mdDialog.show({
		  controller: angular.noop,
		  scope:$scope.$new(),
		  templateUrl: 'templates/registerParent.html',
		  parent: angular.element(document.body),
		  targetEvent: ev,
		  clickOutsideToClose:true,
		  fullscreen: true
		})
		.then(function(answer) {
		  $scope.status = 'You said the information was "' + answer + '".';
		}, function() {
		  $scope.status = 'You cancelled the dialog.';
		});
		$scope.$watch(function() {
		  return $mdMedia('xs') || $mdMedia('sm');
		}, function(wantsFullScreen) {
		  $scope.customFullscreen = (wantsFullScreen === true);
		});
	  };

	$scope.dlgCancel = function() {
	    $mdDialog.cancel();
	};

    $scope.doRegister=function() {

		//take the data, update our 'system' config
		$mdDialog.hide();
	/*	var syscfgfiles = [
	    		{
    			  "name": "parent_email",
			      "data": [ $scope.cp.emailAddress ]
			    },
				{
			      "name": "state",
			      "data": [ "CONFIGURED" ]
			   	}  
			];*/

		for(var i=0; i < $scope.system.files.length; i++)
		{
			if($scope.system.files[i].name == "parent_email")
			{
				$scope.system.files[i].data = [ $scope.cp.emailAddress ];
			}
			else if($scope.system.files[i].name == "state")
            {
                $scope.system.files[i].data = [ "CONFIGURED" ];
            }
			else if($scope.system.files[i].name == "services")
            {
                $scope.system.files[i].data = [ "NEEDRESTART" ];
            }
		}

		configService.save('system', false);

		var passwdfiles = [
			    {
			      "name": ".passwd",
			      //"data": [ $scope.cp.emailAddress + ':' + $scope.cp.realm + ':' +  self.md5.createHash($scope.cp.emailAddress  + ":" + $scope.cp.realm + ":" + $scope.cp.password) ]
			      "data": [ self.md5.createHash($scope.cp.emailAddress  + ":" + $scope.cp.realm + ":" + $scope.cp.password) ]
				},
			];

		$scope.password.files=passwdfiles;
		configService.save('password', false);
    }

    $scope.doLogin=function() {
		blxSessionService.setCredentials($scope.loginParam.emailAddress, $scope.loginParam.password);
		blxSessionService.Login();
		$scope.initLoginParam();
		$scope.loginDlg.$setPristine();
		$scope.loginDlg.$setUntouched();
    };

    configService.bind('system', $scope, false);

	$scope.$on("$destroy", function()
	{
        configService.unbind('system', $scope);
    });
}
]);
