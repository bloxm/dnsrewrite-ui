var bloxmApp = angular.module('bloxm');
bloxmApp.service('configService', [ '$q', '$http', '$timeout', '$interval', '$log', 'md5', configService]);

function configService($q, $http, $timeout, $interval, $log, md5){
    var self=this;
    self.configmap={};
	//Used to determine is configuration is modified.
    self.hashmap={};
    self.modifiedmap={};
    self.bindobjects={};
	self.timerpromise={};
	self.configchanged={};
	self.configloaded={};
	self.privateconfig={};

	self.urls={	false: '/blxadmin/cgi-bin/config.cgi?config=',
				true: '/blxadmin/cgi-bin/private/config.cgi?config='};

    self.configToJSONFilter = function(key, value) 
	{
    	if(key.startsWith('$'))
	        return undefined;
		else if(key.startsWith('_BLX_'))
			return undefined;

    	return value;
    }

	//Two queues, switch them for processing
	self.requestQueues={ true:[], false:[] };
	self.requestQueueIndex=false;

	//The current queue which should be populated
	//The other is being processed
	self.requestQueue=function(){
		return self.requestQueues[self.requestQueueIndex];
	}

	//We only want to run one instance of the queue processing loop
	self.requestProcessing=false;

	//Config requests are added to a queue, so they can be dealt with synchronously to reduce load on the appliance
	self.processrequestqueue=function(fromCallback)
	{
		if(self.requestProcessing){	return;	}
		self.requestProcessing = true;

		//Switch the queue
		self.requestQueueIndex = !self.requestQueueIndex;

		self._processnext();
	}

	self._processnext=function()
	{
		if(self.requestQueues[!self.requestQueueIndex].length)
		{
			self.requestQueues[!self.requestQueueIndex].shift()().then(
				function(res)
				{
					self._processnext();
				},
				function(res)
				{
					$log.info("REQUEST failed");
					self._processnext();
				}
			);
		}
		else
		{
			self.requestProcessing = false;
			if(self.requestQueue().length)
			{
				$timeout(self.processrequestqueue);
			}
		}
	}

    self.saveorget=function(cfgtype)
	{
		if(self.configchanged[cfgtype] && self.checkConfigModified(cfgtype)){
			self.configchanged[cfgtype] = false;
			self.requestQueue().push(function(){ $log.info("Queued save " + cfgtype); return self.save(cfgtype, self.privateconfig[cfgtype]); });
		}else{
			self.requestQueue().push(function(){ $log.info("Queued get " + cfgtype);return self.get(cfgtype, self.privateconfig[cfgtype], true); });
		}
		//Poke the queue
		$timeout(self.processrequestqueue);
	}

	self.stringify=function(obj)
	{
		return JSON.stringify(obj, self.configToJSONFilter);
	}
	self.unbind=function(cfgtype, objParent)
	{
		//Bind this config
        if(self.bindobjects[cfgtype] !== undefined && objParent.cfgService_bindindex !== undefined && objParent.cfgService_bindindex[cfgtype] !== undefined)
        {
			$log.info("Bind objects before is " + self.bindobjects[cfgtype].length);
			self.bindobjects[cfgtype].splice(objParent.cfgService_bindindex[cfgtype], 1);
			delete objParent.cfgService_bindindex[cfgtype]
			$log.info("Bind objects after is " + self.bindobjects[cfgtype].length);

			if(self.bindobjects[cfgtype].length == 0)
			{
				//There is nothing bound to this configuration
				//We should stop polling
				$interval.cancel(self.timerpromise[cfgtype]);
				delete self.bindobjects[cfgtype];
				delete self.timerpromise[cfgtype];
				delete self.configmap[cfgtype];
				delete self.configloaded[cfgtype];
				delete self.hashmap[cfgtype];
				delete self.modifiedmap[cfgtype];
				delete self.configchanged[cfgtype];
				delete self.privateconfig[cfgtype];
			}
        }
		else
		{
			$log.error("Attempt to unbind unbound object " + cfgtype);
		}
	}

    self.bind=function(cfgtype, objParent, bPrivateConfig, callback)
	{
		self.privateconfig[cfgtype] = bPrivateConfig;

		//Bind this config
		if(self.bindobjects[cfgtype] === undefined)
		{
			self.bindobjects[cfgtype] = [];
		}
		
		if(objParent.cfgService_bindindex === undefined)
		{
			objParent.cfgService_bindindex = {};
		}

		if(objParent.cfgService_bindindex[cfgtype])
		{
			//Already bound - shouldn't happen
			$log.error("Object bound multiple times " + cfgtype);
		}
		else
		{
			objParent.cfgService_bindindex[cfgtype] = (self.bindobjects[cfgtype].push({objParent:objParent, callback:callback}) - 1);
		}

		//Start the monitor for this config
		if(!self.timerpromise[cfgtype]){
			self.timerpromise[cfgtype] = $interval(self.saveorget, 10000, 0, true, cfgtype);
			//Kick an immediate read
			self.saveorget(cfgtype);
		}
		else
		{
			objParent[cfgtype] = self.configmap[cfgtype];
		}
    }
	self.isConfigLoaded=function(cfgtype)
	{
		if(self.configmap[cfgtype] != null)
		{
			self.configloaded[cfgtype] = true;
		}
		else
		{
			self.configloaded[cfgtype] = false;
		}
		return self.configloaded[cfgtype];
	}

	self.getlog=function(mac, index)
	{
		var cfgtype = "logs";
		return $http.get(self.urls[ true ] + cfgtype + "&mac=" + mac + "&index=" + index).then(
			function successCallback(response) 
			{
				$log.info("Response received for " + cfgtype);
				return response.data ;
			},
			function errorCallback(response) 
			{
			  $log.error("Error in GETLOG call " + response);
			  return $q.reject(response);
			});
	}
    self.get=function(cfgtype, bPrivateConfig, bForceGet)
	{
		if(!bPrivateConfig)
		{
			bPrivateConfig = false;
		}
		if(!self.configmap.hasOwnProperty(cfgtype) || bForceGet)
		{
			return $http.get(self.urls[ bPrivateConfig ] + cfgtype).then(
			    function successCallback(response) 
			    {
					$log.info("Response received for " + cfgtype);
					//$log.info("JSON: " + JSON.stringify(self.configmap[cfgtype], self.configToJSONFilter));
					//$log.info("HASH: " + md5.createHash(JSON.stringify(self.configmap[cfgtype], self.configToJSONFilter)));
					var newHash = md5.createHash(JSON.stringify(response.data, self.configToJSONFilter));
					if(newHash != self.hashmap[cfgtype])
					{
						self.hashmap[cfgtype] = newHash;
						self.configloaded[cfgtype] = true;
						self.configmap[cfgtype] = response.data ;
						self.modifiedmap[cfgtype] = false;
						if(self.bindobjects[cfgtype]){
							self.bindobjects[cfgtype].map(
									function (item)
									{
										item.objParent[cfgtype] = self.configmap[cfgtype];
										if(item.callback)
										{
											item.callback(cfgtype);
										}
									});
						}
					}
					return self.configmap[cfgtype];
			    },
			    function errorCallback(response) 
			    {
			  	  $log.error("Error in DEVICES call " + response);
				  return $q.reject(response);
			    });
		}
		return {
			then: function(callback) {
			  callback(self.configmap[cfgtype]);
			}
		};
    }
	self.createHash=function(object)
	{
		return md5.createHash(JSON.stringify(object, self.configToJSONFilter));
	}
    //Save the config back
    self.save=function(cfgtype, bPrivateConfig){
		if(self.configmap.hasOwnProperty(cfgtype))
		{
			return $http.post(self.urls[ bPrivateConfig ] + cfgtype, self.configmap[cfgtype]).then(
				function successCallback(response) 
				{
					if(response.data._BLX_success || response.data._BLX_has_config)
					{
						self.configmap[cfgtype] = response.data;
						//$log.info("JSON: " + JSON.stringify(self.configmap[cfgtype], self.configToJSONFilter));
						//$log.info("HASH: " + md5.createHash(JSON.stringify(self.configmap[cfgtype], self.configToJSONFilter)));
						self.hashmap[cfgtype] = self.createHash(self.configmap[cfgtype]);
						self.modifiedmap[cfgtype] = false;
						if(self.bindobjects[cfgtype]){
							self.bindobjects[cfgtype].map(
									function (item)
									{
										item.objParent[cfgtype] = self.configmap[cfgtype];
										if(item.callback)
										{
											item.callback(cfgtype);
										}
									});
						}
					}
			    },
				function errorCallback(response)
				{
				    $log.error("Failed to save config " + cfgtype);
					return self.configmap[cfgtype];
				}
			);
		}
		return undefined;
	}

    self.configChange = function(cfgtype){
		self.configchanged[cfgtype] = true;
		if(!self.timerpromise[cfgtype])
		{
			self.save(cfgtype, self.privateconfig[cfgtype])
		}
	}
	
    self.checkConfigModified = function(cfgtype) {
		bMod = self.modifiedmap[cfgtype];
	    self.modifiedmap[cfgtype] = md5.createHash(JSON.stringify(self.configmap[cfgtype], self.configToJSONFilter)) != self.hashmap[cfgtype];
	    return self.modifiedmap[cfgtype];
    }
}

