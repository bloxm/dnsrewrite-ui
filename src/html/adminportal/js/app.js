
var bloxmApp = angular.module('bloxm', [
  'ngMaterial',
  'ngMessages',
  'ngRoute',
  'angular-md5',
  'ngStorage',
  'bloxmControllers',
  'blxSession'
]);


//Define the left hand side menu 
//link: determines the controller ({link}Ctrl(.js)

bloxmApp.sideMenu = [
    {
      link : 'welcome',
      title: 'Welcome',
      icon: 'account_circle',
	  data: [{
		  requireLogin:false
	  }]
    },
    {
      link : 'people',
      title: 'People',
      icon: 'group',
	  data: [{
		  requireLogin:true,
		  validStates:{CONFIGURED:true}
	  }]
    },
    {
      link : 'messages',
      title: 'Messages',
      icon: 'message',
	  data: [{
		  requireLogin:true,
		  validStates:{CONFIGURED:true}
	  }]
    },
    {
      link : 'devices',
      title: 'Devices',
      icon: 'computer',
	  data: [{
		  requireLogin:true,
		  validStates:{CONFIGURED:true}
	  }]
    }
];

bloxmApp.mgmtLinks = [
{
  link : 'settings',
  title: 'Settings',
  icon: 'settings',
  data: [{
	  requireLogin:false
    }]
},
{
    link : 'router',
    title: 'Router pairing',
    icon: 'settings_ethernet',
    data: [{
	    requireLogin:false,
	    validStates:{PRISTINE:true}
    },
    {
	    requireLogin:true
    }
	]
}
];
bloxmApp.hiddenLinks = [
{
  link : 'routernow',
  altlink : 'router',
  title: 'Router pairing',
  icon: 'settings_ethernet',
  data: [{
	  requireLogin:false,
	  validStates:{PRISTINE:true}
  }]
},
{
  link : 'routerdebug',
  altlink : 'router',
  title: 'Router pairing',
  icon: 'settings_ethernet',
  data: [{
	  requireLogin:false,
	  validStates:{PRISTINE:true}
  }]
},
{
  link : 'pleasewait',
  altlink : 'pleasewait',
  title: 'pleasewait',
  icon: 'none',
  data: []
},
{
  link : 'start',
  altlink : 'pleasewait',
  title: 'pleasewait',
  icon: 'none',
  data: []
}
];
bloxmApp.allLinks=[];

//This binds the links in the menu on the left-hand-side to the html and controller (e.g. devicesCtrl)
bloxmApp.config(['$routeProvider', '$locationProvider',
		  function($routeProvider, $locationProvider) {
			  $routeProvider.otherwise({redirectTo: '/start'});

			  bloxmApp.sideMenu.forEach(function(val) {
				  bloxmApp.allLinks['/' + val.link] = val;

				  $routeProvider.when('/' + val.link, { 
					  templateUrl: 'templates/' + val.link + '.html',
					  controller: val.link + 'Ctrl'
				  });
			  });
			  bloxmApp.hiddenLinks.forEach(function(val) {
				  bloxmApp.allLinks['/' + val.link] = val;
				  var link = val.link;
				  if(val.altlink)
				  {
					  link=val.altlink;
				  }
				  $routeProvider.when('/' + val.link, { 
					  templateUrl: 'templates/' + link + '.html',
					  controller: link + 'Ctrl'
				  });
			  });
			  bloxmApp.mgmtLinks.forEach(function(val) {
				  bloxmApp.allLinks['/' + val.link] = val;
				  var link = val.link;
				  if(val.altlink)
				  {
					  link=val.altlink;
				  }
				  $routeProvider.when('/' + val.link, { 
					  templateUrl: 'templates/' + link + '.html',
					  controller: link + 'Ctrl'
				  });
			  });
//			  $locationProvider.html5Mode(true);
		  }]);

/*bloxmApp.config(
function($stateProvider, $urlRouterProvider)
{

//	$urlRouterProvider.otherwise('/blxadmin/templates/welcome.html');
//	$urlRouterProvider.otherwise(function(){return '/blxadmin/welcome'});
//	$urlRouterProvider.otherwise(function(x){debugger;});
	bloxmApp.sideMenu.forEach(function(val) {
		var link = val.link;
		if(val.altlink)
		{
			link = val.altlink;
		}
	  $stateProvider.state(val.link, { 
		  url: '/blxadmin/', //+val.link+query,
		  templateUrl: 'templates/' + link + '.html',
		  controller: link + 'Ctrl',
		  data:val.data
	  });
	});

	bloxmApp.hiddenStates.forEach(function(val) {

		var link = val.link;
		if(val.altlink)
		{
			link = val.altlink;
		}
	  $stateProvider.state(val.link, { 
		  url: '/blxadmin/',// + val.link,
		  templateUrl: 'templates/' + link + '.html',
		  controller: link + 'Ctrl',
		  data:val.data
	  });
	});
}
);
*/



