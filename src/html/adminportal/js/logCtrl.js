var bloxmControllers = angular.module('bloxmControllers');

bloxmControllers.controller('logCtrl', [ '$scope', '$http', '$interval', '$timeout', '$log', 'configService', '$mdDialog', '$mdMedia', '$sce', '$location', 'macAddr', 'deviceName',

function ($scope, $http, $interval, $timeout, $log, configService, $mdDialog, $mdMedia, $sce, $location, macAddr, deviceName) 
{
	var self = this;
	$scope.macAddr = macAddr;
	$scope.deviceName = deviceName;
	$scope.close = function()
	{
		$mdDialog.hide();
	}

	$scope.logDecode = function(item)
	{
		if(item)
		{
			return angular.fromJson(item);
		}
		return undefined;
	}

    bloxmControllers.addJSONPathHelpers($scope, configService, $log);
    $scope.logs = {};

	self.configService = configService;
    $log.info("LOGCTRL STARTED");
	//MAIN
    self.configService.getlog(macAddr, "1").then(
			function(obj){
				$scope.logs= obj;
			});
}
]);

